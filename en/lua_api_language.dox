/**
\page lua_api_language Language functions

\tableofcontents

\c sol.language lets you get and set the current language
and manage language-specific data.

Like all resources (maps, sounds, etc.),
each language is identified by an id (like \c "en", \c "fr") and has a
human-readable description (like \c "English", \c "Français").
The language id corresponds to the name of a directory
with files translated in this language: dialogs, strings and images.
That directory is located in the \c languages directory of your quest.

The list of languages available in your quest is specified in the
\ref quest_database_file "quest database file".

If there is only one language in your quest, then it is automatically set as
the current language.
Otherwise, if you have several languages, no current language is automatically
set for you, and you need to call
\ref lua_api_language_set_language "sol.language.set_language()"
to be able to use dialogs.

\section lua_api_language_functions Functions of sol.language

\subsection lua_api_language_get_language sol.language.get_language()

Returns the id of the current language.
- Return value (string): The code that identifies the current language, or
  \c nil if no language was set.

\subsection lua_api_language_set_language sol.language.set_language(language_id)

Changes the current language.
- \c language_id (string): The code that identifies the new language to set.
  It must be a valid id as defined in your
  \ref quest_database_file "quest database file".

\subsection lua_api_language_get_language_name sol.language.get_language_name([language_id])

Returns the human-readable description of a language.
- \c language_id (string, optional): A language id
  (no value means the current language).
- Return value (string): The human-readable name of this language.

\remark Equivalent to
  \ref lua_api_main_get_resource_description "sol.main.get_resource_description(\"language\", language_id)"

\subsection lua_api_language_get_languages sol.language.get_languages()

Returns the list of available languages.
- Return value (table): An array of all language ids declared in the quest.

\remark Equivalent to
  \ref lua_api_main_get_resource_ids "sol.main.get_resource_ids(\"language\")"

\subsection lua_api_language_get_string sol.language.get_string(key)

Returns a string translated in the current language.

There must be a current language when you call this function.

Translated strings are defined in the file
\ref quest_language_strings "text/strings.dat"
of the
language-specific directory (e.g. \c languages/en/text/strings.dat).
- \c key (string): Key of the string to get. The corresponding key-value
  pair must be defined in
  \ref quest_language_strings "text/strings.dat".
- Return value (string): The value associated to this key in
  \ref quest_language_strings "text/strings.dat", or \c nil if it does
  not exist.

\subsection lua_api_language_get_dialog sol.language.get_dialog(dialog_id)

Returns a dialog translated in the current language.

There must be a current language when you call this function.

Translated dialogs are defined in the file
\ref quest_language_dialogs "text/dialogs.dat" of the
language-specific directory (e.g. \c languages/en/text/dialogs.dat).
- \c dialog_id (string): Id of the dialog to get.
- Return value (table): The corresponding dialog in the current language,
  or \c nil if it does not exist.
  The dialog is a table with at least the following two entries:
  - \c dialog_id (string): Id of the dialog.
  - \c text (string): Text of the dialog.
  The table also contains all custom entries defined in
  \ref quest_language_dialogs "text/dialogs.dat"
  for this dialog.
  These custom entries always have string keys and string values.

*/
